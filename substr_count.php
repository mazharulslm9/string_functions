<?php

$text = 'This is a test';
echo strlen($text); // 14
echo '</br>';
echo substr_count($text, 'is'); // 2
echo '</br>';
// the string is reduced to 's is a test', so it prints 1
echo substr_count($text, 'is', 3);
echo '</br>';
// the text is reduced to 's i', so it prints 0
echo substr_count($text, 'is', 3, 3);

